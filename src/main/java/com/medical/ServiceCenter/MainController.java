package com.medical.ServiceCenter;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    private DeviceRepo repo;
    private EngineerRepo repo2;

    public MainController(DeviceRepo repo,
                          EngineerRepo repo2) {
        this.repo = repo;
        this.repo2=repo2;
    }

    @GetMapping(value = "/")
    public String root(){
        return "redirect:index";
    }
    @GetMapping(value = "/index")
    public String index(){
        return "index";
    }
    @GetMapping(value = "/catalog")
    public String catalog(Model model){
       List<Device>devices=repo.findAll();
model.addAttribute("devices",devices);
        return "catalog";

    }

    @GetMapping(value = "/engineers")
    public String engineers(Model model){
        repo2.save(new Engineer(2L,"Semen","Tiger","kettle"));
        List<Engineer>engineers=repo2.findAll();
        model.addAttribute("engineers",engineers);
        return "engineers";
    }

    @GetMapping(value = "/back")
    public String back(){
        return "redirect:index";
    }
}
