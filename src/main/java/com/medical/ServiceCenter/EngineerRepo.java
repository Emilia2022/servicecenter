package com.medical.ServiceCenter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EngineerRepo extends JpaRepository<Engineer,Long> {
}
